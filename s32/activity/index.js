let http = require("http");

const app = http.createServer(function (request, response) {
  // Extract the URL path from the request object
  const { url } = request;
  
  // Route: /
  if (url === '/' && request.method == "GET") {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Welcome to Booking System');
  }
  
  // Route: /profile
  else if (url === '/profile' && request.method == "GET") {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Welcome to your profile!');
  }
  
  // Route: /courses
  else if (url === '/courses' && request.method == "GET") {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end("Here's our courses available");
  }
  
  // Route: /addcourse
  else if (url === '/addcourse' && request.method == "POST") {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Add a course to our resources');
  }
  
  // Route: /updatecourse
  else if (url === '/updatecourse' && request.method == "PUT") {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Update a course to our resources');
  }
  
  // Route: /archivecourses
  else if (url === '/archivecourses' && request.method == "DELETE") {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Archive courses to our resources');
    }

});

//Do not modify
//Make sure to save the server in variable called app
if (require.main === module) {
  app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;