// Defines WHEN particular controllers will be used
// Contains all the endpoints that will be used in our API

const express = require("express");


// Creates a ROUTER instance that functions as a middleware and routing system
const router = express.Router();

// The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controllers/taskController");
const task = require("../models/task");

// [SECTION] Routes
// http://localhost:3001/tasks/
router.get("/", (req, res) => {

    // Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
    taskController.getAllTasks().then(resultFromController =>
        res.send(resultFromController));
});

// Route to create a new task
router.post("/", (req, res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Delete Task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

// http://localhost:3001/tasks/649adbacad4f4425ff3a867c
router.delete("/:id", (req, res) => {
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Update Task
router.put("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then
        (resultFromController => res.send(resultFromController));
});

// [SECTION] ACTIVITY

// Create a route for getting a specific task
router.get("/:id", (req, res) => {
    taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Create a route for changing the status of a task to complete
router.put("/:id/complete", (req, res) => {
    taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
});







module.exports = router;