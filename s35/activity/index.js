const express = require("express");
const mongoose = require("mongoose");

// "mongoose" is a package that allows creation of schemas to model our data structures
// Also has access to a number of methods for manipulating our database

const app = express();

const port = 3001;

// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://bihagrudolf:8qA9i7LoqkPq9IYU@wdc028-course-booking.8d6it54.mongodb.net/s35",

    // allows us to avoid any current and future errors while connecting to MongoDB

    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

// Connecting to MongoDB locally
// Allows to handle errors when the initial connection is established

let db = mongoose.connection;

// "console.error.bind" allows us to print errors in the browser console and in the terminal
// "connection error" is the message that wull display if an error is encountered

db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, "We're connected to the cloud database!" output in the console

db.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// // [SECTION] Mongoose Schemas
// // "Schema" is a blueprint for our data
// // "Schema" determine the structure of our documents to be written in the database

// // The "new" keyword is used to create a new instance of a schema
// // "new mongoose.Schema" is the constructor function that creates a new schema
// /*
// SYNTAX:
// const schemaName = new mongoose.Schema({
// });
// */

// const taskSchema = new mongoose.Schema({
//     name: String,
//     status: {
//         type: String,
//         default: "pending"
//     }
// });

// // [SECTION] Mongoose Models
// // "Model" in singular form and first letter is capitalized
// // First parameter of the mongoose model indicates the collection in where to store the data
// // Second parameter is the schema that will be used to create the model

// const Task = mongoose.model("Task", taskSchema);

// /*
// Creating a new task
// 1. Add a functionality to check if there are duplicate tasks
//     - If the task already exists in the database, we return a message
//     - If the task doesn't exist in the database, we add it in the database
// 2. The task data will be coming from the request's body
// 3. Create a new Task object with a "name" field/property
// 4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
// */


// app.post("/tasks", (req, res) => {
//     Task.findOne({ name: req.body.name }).then((result, err) => {
//         if (result != null && result.name == req.body.name) {
//             return res.send("Duplicate task found!");
//         } else {
//             let newTask = new Task({
//                 name: req.body.name
//             });
//             // "save()" is a method that saves the new task in the database
//             newTask.save().then((savedTask, saveErr) => {
//                 if (saveErr) {

//                 } else {
//                     return res.status(201).send("New Task created!");
//                 }
//             })
//         }
//     })
// });


// /*
// Getting all the tasks
// 1. Retrieve all the documents
// 2. If an error is encountered, print the error
// 3. If no errors are found, send a success status back to the client/Postman and return an array of documents
// */

// app.get("/tasks", (req, res) => {
//     Task.find({}).then((result, err) => {
//         if (err) {
//             return console.log(err);
//         } else {
//             return res.status(200).json({ data: result });
//         }
//     })
// });

// [SECTION] ACTIVITY

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
});

const User = mongoose.model('User', userSchema);

app.post('/signup', (req, res) => {
    const { username, password } = req.body;

    User.findOne({ username })
        .then(existingUser => {
            if (existingUser) {
                return res.status(400).send('Duplicate username found');
            }

            if (!username || !password) {
                return res.status(400).send('BOTH username and password must be provided.');
            }

            const newUser = new User({ username, password });
            newUser.save()
                .then(() => {
                    res.status(201).send('New user registered');
                })
        })
});






app.listen(port, () => console.log(`Server running at port ${port}!`));