declaredFunction();
// console.log("Hello World!")

// [SECTION] Functions
// Functions in JS are lines/block of codes that tell our device/application to perform a certain task when called/invoked

// Functions declarations
/*
SYNTAX:
	fucntion functionName() {
		code block (statement)
	}
*/

// function - keyword used to define a JS functions
// printName - function name. 
// Functions are name to be able to be used later in the code
function printName () {
	// function block {} - the statements which comprise the body of the function
	console.log("My name is John");
}

// Function invocation
printName();

// [SECTION] Function declarations vs expressions
// Function Declarations
// A function can be created through function declaration by using FUNCTION keyword and adding a function name

declaredFunction();

function declaredFunction () {
	console.log("Hello World from declaredFunction()!")
}
declaredFunction();

// Function Expressions
// A function can also be stored in a variable
// A function expression is an anonymous function assigned to a variableFunction
// Anonymous function - function without a name

// variableFunction(); cannot be hoisted meaning cannot be first from inititialization

let variableFunction = function() {
	console.log("Hello again!")
}

variableFunction();

// Function Expression are always invoked/called using variable name
let funcExpression = function funcName() {
	console.log("Hello from the other side!");
}

// funcName(); result:error funcname not defined
funcExpression();


// You can reassign function declarations and function expressions to new anonymous functions

declaredFunction = function() {
	console.log("Updated declaredFunction!");
}
declaredFunction();

funcExpression = function() {
	console.log("Updated funcExpression!")
}
funcExpression();

const constantFunc = function() {
	console.log("Initialized with const!")
}
constantFunc();

// constantFunc = function() {
// 	console.log("Cannot be reassigned!");
// }
// constantFunc();

// [SECTION] Function scoping
// Scope is the accessibility/visibility of variables
// JS has 3 types of scope
/*
JS has 3 types of scope
1. local/block scope
2.global scope
3.function scope
*/

{
	let localVar = "Armando Perez";
	console.log(localVar);//only available inside blocks
}

let globalVar = "Mr. Worldwide";

console.log(globalVar);

function showNames() {

	// function scope variables
	var functionVar = "Joe";
	const functionConst = "john";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}
showNames();
// console.log(functionVar); will result to error
// console.log(functionConst); will result to error
// console.log(functionLet); will result to error


// Global Scope Variable
let globalName = "Alex";

function myNewFunction2() {
	let nameInside = "Renz";

	console.log(globalName);
}
myNewFunction2();


// Nested Functions
// You can create another funciton inside a function

function myNewFunction() {
	let name = "Rudolf";

	function nestedFunction() {
		let nestedName = "Bihag";
		console.log(name);
	}
	nestedFunction();
}
myNewFunction();



// alert("Hello World!"); //This will run immediately when the page loads

// function showSampleAlert() {
// 	alert("Hello, User!");
// }
// showSampleAlert();

// console.log("I will only log in the console when the alert is dismissed.");

//[SECTION] Using prompt()
// prompt() allows us to show a small window at the top of the browser to gather user input

let samplePrompt = prompt("Enter your name");
console.log("Hello, " + samplePrompt);

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name.");
	let lastName = prompt("Enter you last name.");
	console.log("Hello, " + firstName +" "+ lastName + "!");
	console.log("Welcome to my page!")
}
printWelcomeMessage();

// [SECTION] Function Naming Conventions

// Function names should be definitive of the task it will perform. 
// It usually contains a verb
function getCourses() {
	let courses = ["Science 101", "Math 101", "English 101"]
	console.log(courses);
}
getCourses();

// Name your functions in small caps
// Follow camelCase when naming variables
function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");
}
displayCarInfo();

// Avoid generic names to avoid confusion within your code
function get() {
	let name = "Jamie";
	console.log(name);
}
get();


// Avoid pointless and inappropriate function name
function foo() {
	console.log(25%5);
}
foo();
