// console.log("Hello World!");

// [SECTION] Arithmetic Operators
// Arithmetic Operators allow mathematical operations

let x = 4;
let y = 12;

let sum = x + y;
console.log("Result of addition operator: " + sum); // result: 16

let difference = x - y;
console.log("Result of subtraction operator: " + difference); //result: -8

let product = x * y;
console.log("Result of multiplication operator: " + product); //result: 48

let qoutient = x / y;
console.log("Result of division operator: " + qoutient); //result: 0.33

let remainder = x % y;
console.log("Result of modulo operator: " + remainder); //result:

// [SECTION] Assignment Operators
// Basic Assignment Operator (=)
// The Assignment operator assigns the value of the right operand to a variable

let	assignmentNumber = 8;
console.log(assignmentNumber); // 8

// Addition Assignment Operator (+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable

assignmentNumber = assignmentNumber + 2; //longhand
console.log("Result of addition assignment operator: " + assignmentNumber); //result: 10

assignmentNumber += 2; //shortcut
console.log("Result of addition assignment operator: " + assignmentNumber); //result: 12

// Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation " + mdas); //result: 0.6000000000000001

let pemdas = 1 + (2-3) * (4 / 5);
console.log("Result of pemdas operation " + pemdas); //result: 0.19999999999999996

// Increment(++) and Decrement (--)
// Operators that add or subtract calues by 1 and reassigns the value of the variable where the increment and decrement was applied to

let z = 1;

let increment = z++;
console.log("Result of increment: " +z); //result: 2

let decrement = z--;
console.log("Result of decrement: " +z); //result: 1

// [SECTION] Type Coersion
// Type coersion is the automatic or implicit conversion of values from one data type to another
// Values are automatically converted from one data type to another in order to resolve operations

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //result: 1012
console.log(typeof coercion); //result: string (typeof-displays what is the type string/number)

// [SECTION] Comparison Operators
// Equality Operator (==)
// Checks whether the operands are equal or have the same content
console.log("Equality Operator");
console.log(1 == 1); //result: True
console.log(1 == '1'); //result: True
console.log("juan" == 'juan'); //result: True
console.log(0 == false); //result: True 0=false 1=true

// Strict Equality Operator (===)
// Check whether the operands are equal and have the same content and also compares the data types of the two values
console.log("Strict Equality Operator");
console.log(1 === 1); //result: True
console.log(1 === '1'); //result: False
console.log("juan" === 'juan'); //result: True
console.log(0 === false); //result: False 0=false 1=true

// Inequality Operator (!=)
// Checks whether the operands are NOT equal/have different content
console.log("Inequality Operator");
console.log(1 != 1); //result: False
console.log(1 != '1'); //result: False
console.log("juan" != 'juan'); //result: False
console.log(0 != false); //result: False 0=false 1=true

//Strict Inequality Operator (!==)
//Checks whether the operands are NOT equal/have different content and also compares that data types of the two values
console.log("Strict Inequality Operator");
console.log(1 !== 1); //result: False
console.log(1 !== '1'); //result: True
console.log("juan" !== 'juan'); //result: False
console.log(0 !== false); //result: True 0=false 1=true

// [SECTION] Relational Operations
let a = 50;
let b = 65;

//GT(>) Greater Than Operator
let isGreaterThan = a > b; //result: False

//LT(<) or Less Than Operator
let isLessThan = a < b; //result: True

//GTE or Greater Than or Equal Operator (>=)
let isGTorGTE = a >= b; //result: False

//LTE or Less Than or Equal Operator (<=)
let isLTorLTE = a <= b; //result:True

console.log("Relational Operations")
console.log(isGreaterThan)
console.log(isLessThan)
console.log(isGTorGTE)
console.log(isLTorLTE)

// [SECTION] Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical And Operator (&&)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: " + allRequirementsMet);
//result: False

// Logical OR Operator (||)
// Returns true if one of the operands is true
let someRequirementsMet = isLegalAge || isRegistered
console.log("Result of Logical OR Operator: " + someRequirementsMet); //result: True

// Logical NOT Operator (!)
// Returns opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet); //true