// Use the 'require' directive to load the express module/package
const express = require("express");

// Create an application using express
// "app" is our server
const app = express();

const port = 3000;

// Methods used from express.js are middleware
// Middleware is software that provides services and capabilities to applications outside of what's offered by the operating system

// allows your app to read json data
app.use(express.json());

// allows your app to read data from any forms
app.use(express.urlencoded({extended: true}));

// [SECTION] Routes
// GET
app.get("/greet", (request, response) => {
	// "response.send" method to send a response back to the client
	response.send("Hello from the /greet endpoint!")
});

// POST
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

// // Simple Registration

let users = [];

app.post("/signup", (request, response) => {
	if (request.body.username !== "" && request.body.password !== "") {
		users.push(request.body);

		response.send(`User ${request.body.username} successfully registered!`);
	} else {
		response.send("Please input BOTH username and password!");
	}
});

// [SECTION] Activity

// Create a GET route that will access the "/home" route
app.get("/home", (request, response) => {
	response.send("Welcome to the home page!");
});

// Create a GET route that will access the "/users" route

app.get("/users", (request, response) => {
  response.send(users);
});

// Create a DELETE route that will access the "/delete" route

app.delete("/delete-user", (request, response) => {
  const usernameToDelete = request.body.username;
  let message;

  if (users.length > 0) {
    let userFound = false;

    for (let i = 0; i < users.length; i++) {
      if (users[i].username === usernameToDelete) {
        users.splice(i, 1);
        message = `User ${usernameToDelete} has been deleted.`;
        userFound = true;
        break;
      }
    }

    if (!userFound) {
      message = "User does not exist.";
    }
  } else {
    message = "No users found.";
  }

  response.send(message);
});


app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
}
);





