const http = require("http");

// mock database
let directory = [
    {
        "name": "Rudolf",
        "email": "rudolf@mail.com"
    },
    {
        "name": "Lowe",
        "email": "lowe@mail.com"
    }
]

http.createServer(function (request, responce) {

    // GET METHOD
    if (request.url == "/users" && request.method == "GET") {
        // Sets response output to JSON data type
        responce.writeHead(200, { 'Content-Type': 'application/json' })
        // write() is a methond in node.js that is used to write data to the reponse body in HTTP server
        // JSON.stringify() method converts the string input to JSON
        responce.write(JSON.stringify(directory));
        responce.end()
    };

    // POST METHOD
if(request.url == "/users" && request.method == "POST"){
	let requestBody = '';
		request.on('data', function(data){
			requestBody += data;
		});
	 
		request.on('end', function(){
	    requestBody = JSON.parse(requestBody);
	    
	    let newUser = {
	    	"name" : requestBody.name,
	    	"email" : requestBody.email
	    }

	    directory.push(newUser)
	    console.log(directory);

		response.writeHead(200,{'Content-Type': 'application/json'});
		response.write(JSON.stringify(newUser));
		response.end();
		});
	}



}).listen(3000);

console.log('Server is running at localhost: 3000')

