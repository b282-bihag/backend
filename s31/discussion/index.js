
// Create a server

// "require" directive to load node.js modules 
// "http" module that lets the node.js transfer data using the Hyper Text Transfer Protocol (HTTP)
let http = require("http");

// "createServer()" method used to create an HTTP server that listens to requests on a specific port and give responses back to the client
// "request" - messages sent by the client (usually a web browser)
// "response" - messages sent by the server as an answer

http.createServer(function (request, response) {

    // writehead() method used to set a status code for response and set the content type of the response
    // 200 - successful request
    // 404 - page not found
    response.writeHead(200, { "Content-Type": "text/plain" });
    // end() method used to send the response with text content 'Hello World!'
  response.end("Hello World!");

    // listen(portNumber) method used to start the server
}).listen(4000);

console.log("Server running at localhost: 4000");



