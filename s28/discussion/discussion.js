// CRUD Operations

// Create Operation
// insertOne() - inserts one document to the collection
db.users.insertOne({
	"firstName": "John",
	"lastName": "Smith"
});

// insertMany() - inserts multiple documents to the collection
db.users.insertMany([
	{"firstName": "John", "lastName": "Doe"},
	{"firstName": "Jane", "lastName": "Doe"}

]);

// Read Operation
// find() - get all the inserted users
db.users.find();

// Retrieving specific documents
db.users.find({ "lastName": "Doe" });



// Update operation
// updateOne() - modify one document
db.users.updateOne(
{
	"_id": ObjectID("648af8e40cf7f9b3f1d753ed")
},
{
	$set: {
		"email": "johnsmith@gmail.com"
	}
}
);

// updateMany() - modify multiple documents
db.users.updateMany(
{
	"lastName": "Doe"
},
{
	$set: {
		"isAdmin": false
	}
}
);

// Delete Operation
// deleteMany - deletes multiple documents
db.users.deleteMany({"lastName": "Doe"});

// deleteOne - deletes single document
db.user.deleteOne({"_id": ObjectId("648af8e40cf7f9b3f1d753ed")});