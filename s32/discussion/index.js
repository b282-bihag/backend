const http = require("http");

// method: GET
// data to be retreived 
http.createServer(function (req, res) {
  if (req.url == "/items" && req.method == "GET") {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Data retrieved from the database');
  }

  // method: POST
  // data to be sent to the database
  if (req.url == "/items" && req.method == "POST") {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Data to be sent to the database');
  }

}).listen(4000);

console.log('Server is running at localhost: 4000')

