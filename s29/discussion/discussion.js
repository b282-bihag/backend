// Comparison Query Operators

// $gt / $gte operator

db.users.find({age: {$gt: 50}});
db.users.find({age: {$gte: 50}});

// $lt / $lte operator

db.users.find({age: {lt: 50}}); 
db.users.find({age: {lte: 50}}); 

// $ne operator
/*
	Allows us to find documents that have field number values not equal to a specified value
	-Syntax
	db.collectionName.find({filed: {$ne: value}});
*/
db.users.find({age: {$ne:82}});

// $in operator
/*
	Allows us to find documents with specific match criteria one field using different values

	-Syntax
		db.collectionName.find({field: {$in: value}});
*/
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});

db.users.find({courses: {$in: ["HTML", "React"]}});

// Logical Query Operators

// $or operator
/*
	Allows us to find documents that match a single criteria from multiple provided search criteria
	-Syntax
	db.collectionNAme.find({$or: [{fieldA: valueA}, {fieldB:}valueB]});
*/
db.users.find({$or: [{firstName: "Neil"},{age: 21}]});

// $and operator
/*
	Allows us to find documents matching multiple criteria in a single field
	-Syntax
	db.collectionName.find({$and: [{fieldA: valueA},{fieldB: valueB}]});
*/
db.users.find({$and: [{age: {$ne: 82}} ,{age: {$ne:76}} ]});

// Field Projection

// Inclusion
/*
	-Allows us to include or add a specific fields only when retrieving documents
	-the value to provide is 1 to denote that the field is being included or shown
	-Syntax
	db.users.find({criteria},{field:1});
*/

db.users.find(
	{firstName: "Jane"},
	{firstName: 1, lastName: 1, contact:1}
);

// Exclusion
/*
	-Allows us to exclude or remove specific fields only when retrieving documents
	-the value to provide is 0 to denote that the field is being excluded or not shown
	-Syntax
	db.users.find({criteria},{field:0});
*/

db.users.find(
	{firstName: "Jane"},
	{contact:0, department:0}
);

// Suppressing the ID field
/*
	-Allows us to exclude the "_id" field when retrieving documents
	-when we are using field projection, field inclusion and exclusion may not be used at the same time
	-Excluding the "_id" field is the only exception to this rule
	-Syntax
	db.users.find({criteria}, {_id:0})
*/

db.users.find(
	{firstName: "Jane"},
	{firstName: 1, lastName: 1, contact:1, _id:0}
);

// Returning Specific Fields in Embedded documents
db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1
}
);

// Project Specific Array Elements in the returned Array
// The $slice operator allows us to retrieve elements that matches the search criteria
db.users.find(
{
	"namearr":
	{
		namea: "juan"
	}
},
	{
		namearr: {$slice: 1}
	}
);

// Evaluation Query Operators
// $regex operator
/*
	-Allows us to find documents that match a specific string pattern using regular expressions(reg.ex.)
	-Syntax
		db.users.find({field: $regex:'pattern', $options: 'optionValue'});
*/
// case sentsitive query
db.users.find({firstName: {$regex: 'N'}});

// case insentsitive query
db.users.find({firstName: {$regex: 'j', $options: 'i'}});