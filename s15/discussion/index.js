// [SECTION] Syntax, Statements, and Comments

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the writted code

/*
There are two types of comment:
	1. Single-line comment - denoted by two slashes (ctrl + /)
	2. Multi-line - denoted by a slash and asterisk (ctrl + shift + /)
*/

/*Statements
Statements in programming are instruction that we tell the computer to perform
*/
console.log("Hello World!"); //logs "Hello World!" to the console in web browser inspect dev tool

// Whitespace
// Whitespace (basically, spaces and line breaks) can impact functionality in many computer language but NOT in JS.
console.      log("Hello World!");

console

.

log
(
	"Hello World!"
	);

// Syntax
// In programming, it is the set of rules that describes how statements must be constructed.
let myVariables;

// [SECTION] Variables
// It is used to contain data
// Any information that is used by an application is stored in what we call a "memory"

// Declaring variables - tells our device that a variable name is created and is ready to store data
/*
Syntax:
 let/const variableName;
*/
// let myVariables;
// console.log(myVariables);

/*console.log(hello); // result:Uncaught SyntaxError: Identifier 'myVariables' has already been declared (at index.js:43:5). this is wrong let hello; should be first
let hello;*/



/*
Naming Convention
Camel Case: Words are joined together without spaces, and each word, except the first one, starts with a capital letter. For example, myVariableName.

Pascal Case: Similar to camel case, but the first letter of each word is capitalized. For example, MyVariableName.

Kebab Case: Words are joined together with hyphens (-) and all letters are lowercase. For example, my-variable-name.

Snake Case: Words are written in all lowercase letters and separated by underscores (_). For example, my_variable_name
*/

// Declaring and Initializing variables
// let productName; //declaring
/*
Syntax:
	//let/const variableName = value;
*/
let productName = "desktop computer"; //initializing variables - the instance when a variable is given it's initial value/starting value
console.log(productName); //result: desktop computer

let productPrice = 18999;
console.log(productPrice) //result: 18999

const interest = 3.539;
console.log(interest);

// Reassigning variable values
// reassigning variables means changing its initial or previous value into another value
/*
SYNTAX:
	variableName = newValue;
*/
productName = "Laptop"; //from line 68
console.log(productName) // result: laptop instead of desktop computer


//let variable cannot be re-declared within its scope
// let friend ="Kate";
// let friend ="Jane"; result: Uncaught SyntaxError

// const variable cannot be updated or re-declared
// const interest = 3.539;
// interest = 4.489; //result: Uncaught TypeError: Assignment to constant variable.
//     at index.js:93:10
// console.log(interest);

// Reassigning variables vs Initializing variables
// Declaration
let supplier;

// Initialization
supplier = "John Smith Tradings"
console.log(supplier); // result: John Smith Tradings

// Reassignment
supplier = "Zuitt Store"
console.log(supplier) // result:Zuitt Store

// var - can also be used in declaring a variable
// Hoisting is JS default behavior of moving initialization to top
a = 5
console.log(a);
var a; //var is no longer used since its very buggy

// let/const local/global scope
// scope essentially means where these variables are available for use
// let and const are blocked scoped
// A block is a chunk of code bounded by {}

let outerVariable = "hello";
{
	let innerVariable = "hello again";
	console.log(innerVariable);
}
console.log(outerVariable);
// console.log(innerVariable); // result: index.js:125 Uncaught ReferenceError: innerVariable is not defined

// Multiple variable declaration
let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand);

// Using a variable with a reserved keyword
// const let = 'hello'
// console.log(let) //result: Uncaught SyntaxError: let is disallowed as a lexically bound name

// [SECTION] Data types
// Strings are series of characters that create a word, phrase, sentence or anything related to creating text
// Strings in JS can be written using either a (' ') or (" ") qoute
let country = 'Philippines';
let province = "Metro Manila";

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol
let fullAdress = country + " , " + province;
console.log(fullAdress);

let greeting = " I live in the " + country;
console.log(greeting);

// Escape character (\)
// (\n) refers to creating a new line in between text
let mailAddress = 'Metro Manila\n\nPhilippines'
console.log(mailAddress)

let message = "John's employees went home early";
console.log(message)

message = 'John\'s employess went home early';
console.log(message);

// Number
// interger/Whole Number
let headCount = 26;
console.log(headCount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponintial Notation
let planetDistance = 2e10;
console.log(planetDistance)

// Combining number data and strings
console.log("John's grade last quarter is " + grade);

// boolean
//normally used to store values relating to  the state of certain things

let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct " + inGoodConduct);

// Arrays
// Arrays are special kind of object data type that's used to store multiple values
/*
Syntax:
	let/const arrayName = [elementA, elementB, elementC....]
*/

// similar data types
let grades = [100,90.8,85.5]
console.log(grades)

// different data type
let details = ["john", "Smith", 32, true];
console.log(details);

// Objects is a special kind of data type that's used to mimic real word items
/*
Syntax
	let/const objectName = {
	propertyA: value,
	propertyB: value,
	}
*/

let person = {
	fullname: " Juan Dela Cruz",
	age:35,
	isMarried: true,
	contact: ["121232134", "1213 1235"],
	address: {
		houseNumber: '345',
		city: 'Manila ',
	}
}
console.log(person)

console.log(typeof grade);
console.log(typeof grades);
console.log(typeof person);


// Null
// It is used to intentionally express the abscene fo a value in a variable declaration/initialization.
let spouse = null;
console.log(spouse);


// Undifined
// Represents the state of a variable that has been declared but without an assigned value
let fullName;
console.log(fullName);