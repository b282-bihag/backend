// Contains instructions on how your API will perform its intended tasks
// All the operations it can do will be placed in this file

const { get } = require("http");
const Task = require("../models/task");

module.exports.getAllTasks = () => {
    return Task.find({}).then((result => {
        return result;
    }))
};

// Controller creating a task
module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        name: requestBody.name
    })
    return newTask.save().then((task, error) => {
        if (error) {
            console.log(error)
            return false;
        } else {
            return task
        };
    });
};

// Controller deleting a task
module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
        if (error) {
            console.log(error)
            return false;
        } else {
            return "Deleted task."
        };
    });
};

// Controller updating a task
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        }

        result.name = newContent.name;

        return result.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.log(saveErr);
                return false;
            } else {
                return "Task updated.";
            };
        });
    });
};

// [SECTION] ACTIVITY

// Create a controller function for retrieving a specific task
module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return result;
        };
    });
}

// Create a controller function for changing the status of a task to complete
module.exports.completeTask = (taskId) => {
    return Task.findById(taskId).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            result.status = "complete";
            return result.save().then((updatedTask, saveErr) => {
                if (saveErr) {
                    console.log(saveErr);
                    return false;
                } else {
                    return "Task completed.";
                };
            });
        };
    });
}