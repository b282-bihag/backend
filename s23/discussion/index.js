// console.log("Hello World!");

// [section] objects
// an object is a data type that is used to represent real world objects
// information stored in objects are represented in a "key:value" pair

/*
syntax:
	let objectName = {
		keyA: valueA,
		KeyB: valueB,
		......
	}
*/

let cellphone = {
	name: 'Iphone13proMax',
	manufactureDate: 2021

};
console.log("result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
/*
syntax:
	function ObjectName(keyA, keyB) {
		this.keyA = KeyA;
		this.keyB = KeyB;
	}
*/

function Laptop(name, manufactureDate) {
	// "this" keyword allows to assign a new object's properties by associating them with values received from constructor function's parameter
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// "new" operator creates an instance of an object
let laptop = new Laptop("Lenovo", 2008);
console.log(laptop);

let myLaptop = new Laptop("Toshiba", 2022);
console.log(myLaptop);

// [section] Accessing object properties
// using dot notation
console.log(myLaptop.name);

// using square bracket notation
console.log(myLaptop['manufactureDate']);

// accessing array objects
let array = [laptop, myLaptop];

// square bracket notation
console.log(array[0]['name'])

// dot notation
console.log(array[0].manufactureDate)

// [section] Initializing/Adding, Deleting, Reassigning object properties

let car = {};

// initializing/adding object properties using dot notation
car.name = "Tesla";
console.log(car);

// initializing/adding object properties using square bracket notation
car['manufactureDate'] = 2019
console.log(car)

// deleting object properties
delete car['manufactureDate'];
console.log(car);

// Reassigning object properties
car.name = "Jaguar";
console.log(car);

// [section] object methods
// a method is a function which is a property of an objects

let person = {
	name: "John",
	talk: function() {
		console.log("Hello my name is " + this.name);
	}
}
console.log(person);
person.talk();

// [section] real world application of objects
// using object literals

let myPokemon = {
	name: "rockruff",
	level: 99,
	health: 250,
	attack: 350,

	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon);
myPokemon.tackle();
myPokemon.faint();

// using constructor function

function Pokemon (name, level) {

	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;


	// method
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is now reduced to " + Number(target.health - this.attack));
	}
	this.faint = function() {
		console.log(this.name + ' fainted.');
	}
}

let pickachu = new Pokemon('Pickachu', 16);
console.log(pickachu);

let rockruff = new Pokemon('Rockruff', 24);
console.log(rockruff);

pickachu.tackle(rockruff);
pickachu.faint();

